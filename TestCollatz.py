#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

"""
TestCollatz.py
"""

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    """Class to test Collatz.py."""

    # ----
    # read
    # ----

    def test_read(self):
        """Tests the collatz_read function."""
        string = "1 10\n"
        i, j = collatz_read(string)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """Test 1 to 10"""
        max_cycle = collatz_eval(1, 10)
        self.assertEqual(max_cycle, 20)

    def test_eval_2(self):
        """Test 100 to 200"""
        max_cycle = collatz_eval(100, 200)
        self.assertEqual(max_cycle, 125)

    def test_eval_3(self):
        """Test 201 to 210"""
        max_cycle = collatz_eval(201, 210)
        self.assertEqual(max_cycle, 89)

    def test_eval_4(self):
        """Test 900 to 1000"""
        max_cycle = collatz_eval(900, 1000)
        self.assertEqual(max_cycle, 174)

    def test_eval_5(self):
        """Test 950 to 2000"""
        max_cycle = collatz_eval(950, 2000)
        self.assertEqual(max_cycle, 182)

    def test_eval_6(self):
        """Test 1000 to 10000"""
        max_cycle = collatz_eval(1000, 10000)
        self.assertEqual(max_cycle, 262)

    def test_eval_7(self):
        """Test 1 to 2"""
        max_cycle = collatz_eval(1, 2)
        self.assertEqual(max_cycle, 2)

    def test_eval_8(self):
        """Test 50 to 50"""
        max_cycle = collatz_eval(50, 50)
        self.assertEqual(max_cycle, 25)

    def test_eval_9(self):
        """Test 999999 to 1"""
        max_cycle = collatz_eval(999999, 1)
        self.assertEqual(max_cycle, 525)

    def test_eval_10(self):
        """Test 1 to 999999"""
        max_cycle = collatz_eval(1, 999999)
        self.assertEqual(max_cycle, 525)

    def test_eval_11(self):
        """Test 999999 to 999999"""
        max_cycle = collatz_eval(999999, 999999)
        self.assertEqual(max_cycle, 259)

    def test_eval_12(self):
        """Test 1 to 500000"""
        max_cycle = collatz_eval(1, 500000)
        self.assertEqual(max_cycle, 449)

    def test_eval_13(self):
        """Test 500001 to 999999"""
        max_cycle = collatz_eval(500001, 999999)
        self.assertEqual(max_cycle, 525)

    def test_eval_14(self):
        """Test 10 to 11"""
        max_cycle = collatz_eval(10, 11)
        self.assertEqual(max_cycle, 15)

    def test_eval_15(self):
        """Test 50 to 100"""
        max_cycle = collatz_eval(50, 100)
        self.assertEqual(max_cycle, 119)

    # -----
    # print
    # -----

    def test_print(self):
        """Tests the collatz_print function."""
        string_io = StringIO()
        collatz_print(string_io, 1, 10, 20)
        self.assertEqual(string_io.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """Tests the collatz_solve function."""
        io_input = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        string_io = StringIO()
        collatz_solve(io_input, string_io)
        self.assertEqual(
            string_io.getvalue(),
            "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()
