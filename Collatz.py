#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

"""
Collatz.py
"""
from typing import IO, List, Dict

CACHE: Dict[int, int] = {}

# ------------
# collatz_read
# ------------


def collatz_read(string: str) -> List[int]:
    """
    read two ints
    string is a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    split_string = string.split()
    return [int(split_string[0]), int(split_string[1])]


def calculate_cycle_length(num: int) -> int:
    """
    read one int
    calculate the cycle length of the int
    returns the result as an int
    """
    assert num > 0
    cycle_length = 1
    while num > 1:
        if num in CACHE:
            # If we have n in the cache, then we can subtract 1 because the
            # cache access takes care of the rest of the necessary calcuation.
            return cycle_length - 1 + CACHE[num]
        if(num % 2) == 0:
            num = num >> 1
            cycle_length += 1
        else:
            num = num + (num >> 1) + 1
            cycle_length += 2
    assert cycle_length > 0
    return cycle_length

# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i one   end of the range, inclusive
    j other end of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert 0 < i < 1000000
    assert 0 < j < 1000000
    max_cycle = 0
    start = 0
    end = 0
    # Determine the start and end of the loop.
    #   1. Choose the bigger of i,j as the end of the loop
    #   2. By default, the lower value is the start. However, if start < mid
    #      when mid =(end/2) + 1, then
    #      max_cycle_length(start, end) = max_cycle_length(mid, end),
    #      so we can cut the work in half by setting the start to mid.
    if i < j:
        start = i
        end = j
    else:
        start = j
        end = i
    mid = end >> 1
    if start < mid:
        start = mid
    for num in range(start, end + 1):
        # If we have the value cached, access the value.
        if num in CACHE:
            collatz = CACHE[num]
        # If first time calculating, access the value and then cache it.
        else:
            collatz = calculate_cycle_length(num)
            CACHE[num] = collatz
        # Update the max cycle length.
        if collatz > max_cycle:
            max_cycle = collatz
    assert max_cycle > 0
    return max_cycle

# -------------
# collatz_print
# -------------


def collatz_print(writer: IO[str], i: int, j: int,
                  max_cycle_length: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    writer.write(str(i) + " " + str(j) + " " + str(max_cycle_length) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(reader: IO[str], writer: IO[str]) -> None:
    """
    reader a IO reader
    reader a IO writer
    """
    for string in reader:
        i, j = collatz_read(string)
        max_cycle = collatz_eval(i, j)
        collatz_print(writer, i, j, max_cycle)
